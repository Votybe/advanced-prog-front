import { createRouter, createWebHistory } from "vue-router";
import NotFound from "./pages/NotFound.vue";
import DetailStage from "./pages/DetailStage.vue";
import ListIntership from "./pages/ListIntership.vue";
import AddIntership from "./pages/AddInternship.vue";
import Connexion from "./pages/connexion.vue";

const routes = [
  {
    path: "/",
    name: "ListIntership",
    component: ListIntership,
  },
  {
    path: "/detail-stage/:identifiant",
    name: "DetailStage",
    component: DetailStage,
  },
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: NotFound,
  },

  {
    path: "/addIntership",
    name: "AddIntership",
    component: AddIntership,
  },
  {
    path: "/connexion",
    name: "Connexion",
    component: Connexion,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
