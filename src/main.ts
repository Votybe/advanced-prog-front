import { createApp } from "vue";
import "./style.css";
import App from "./App.vue";
import router from "./router";
import VCalendar from "v-calendar";
import store from "./stores/index";

/* import the fontawesome core */
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { fas } from "@fortawesome/free-solid-svg-icons";

library.add(fas);

/* add font awesome icon component */
createApp(App)
  .component("font-awesome-icon", FontAwesomeIcon)
  .use(router)
  .use(store)
  .use(VCalendar, {})
  .mount("#app");
