export interface StageType {
  Gr: string;
  Nom: string;
  Cdc: string;
  Fiche: string;
  Visite: string;
  Eval: string;
  Sondage: string;
  Web: string;
  Rapport: string;
  Rendu: string;
  Sout: string;
  Planif: string;
  Faite: string;
  Debut: string;
  Fin: string;
  Entr: string;
  Mds: string;
  Adresse: string;
  Note: string;
  Tech: string;
  Note1: string;
  Com: string;
  identifiant: string;
  Com1: string;
}
