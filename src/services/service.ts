// filters.service.js
function filterByOption(internships: any, selectedOption: any): any {
  if (selectedOption === "") return internships;
  return internships.filter(
    (stage: any) => stage.promotion === selectedOption
  );
}

function filterByEntreprise(internships: any, entrepriseFiltre: any) {
  if (entrepriseFiltre === "") return internships;

  return internships.filter((stage: any) =>
    stage.company
      .toLowerCase()
      .includes(entrepriseFiltre.toLowerCase())
  );
}

export { filterByOption, filterByEntreprise };
