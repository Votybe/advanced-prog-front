// store.ts
import axios from "axios";
import { createStore } from "vuex";

export default createStore({
  state: {
    internship: {
      user: {
        id: "",
        group: "",
        name: "",
      },
      docs: {
        cdc: false,
        form: false,
        visit: false,
        evaluation: false,
        survey: false,
        web: false,
        report: false,
        delivery: false,
        defense: false,
        planning: false,
        done: false,
      },
      enterprise: {
        start: new Date(),
        end: new Date(),
        company: "",
        address: "",
      },
      results: {
        techGrade: 0.0,
        comGrade: 0.0,
      },
    },
    internships: [],
    isConnected: false,
    userConnected: "",
  },
  getters: {
    getInternships: (state) => state.internships,
    getUser: (state) => state.userConnected,
  },
  actions: {
    async fetchInternships({ commit }) {
      console.log("this.getters.getUser()",this.getters.getUser.id);
      await axios.get(`http://localhost:8080/advanced-1.0-SNAPSHOT/internshipByTutorIdServlet-servlet?id=`+this.getters.getUser.id)
      .then(res => {
        commit("SET_INTERNSHIPS", res.data);
      })
      .catch(error => {
        console.log(error);
      });
    },
  },
  mutations: {
    SET_INTERNSHIPS(state, internships) {
      state.internships = internships;
    },
    SET_CONNECTED(state) {
      state.isConnected = true;
    },
    SET_DISCONNECTED(state) {
      state.isConnected = false;
    },
    SET_USER(state, userConnected) {
      state.userConnected = userConnected;
    },
  },
  modules: {},
});
